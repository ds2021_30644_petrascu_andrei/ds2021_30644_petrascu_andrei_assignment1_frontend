import React from 'react';
import {Button, Form, FormGroup, Label, Input, Col} from 'reactstrap';
import Select from "react-select";
import * as API_USERS from "../api/person-api";
import * as API_ROLES from "../../role/api/role-api";
import {useHistory} from 'react-router-dom';


class PersonAddUpdateForm extends React.Component {
    state = {
        id: 0,
        username: '',
        password: '',
        name: '',
        birth: '',
        address: '',
        role: '',
        roleData: [],

        error: null,
        errorStatus: 0
    }

    onChange = item => {
        this.setState({[item.target.name]: item.target.value})
    }

    onChangeRole = item => {
        this.setState({role: item.value})
    }

    addPerson() {
        let item = {
            username: this.state.username,
            password: this.state.password,
            name: this.state.name,
            birth: this.state.birth,
            address: this.state.address,
            role: this.state.role
        };

        return API_USERS.postPerson(item, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted person with id: " + result);
                this.props.reload();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });

    }

    editPerson() {
        let item = {
            id: this.state.id,
            username: this.state.username,
            password: this.state.password,
            name: this.state.name,
            birth: this.state.birth,
            address: this.state.address,
            role: this.state.role
        };

        return API_USERS.postPerson(item, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                this.props.reload();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    submitAddForm = e => {
        this.addPerson();
        this.props.toggle();
        this.props.reload();
    }

    submitEditForm = e => {
        this.editPerson();
        this.props.toggle();
        this.props.reload();
    }

    fetchRoles() {
        return API_ROLES.getRoles((result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    roleData: result
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    componentDidMount() {
        this.fetchRoles();
        /*if item exists => 'Edit' operation*/
        if (this.props.item) {
            const {id, username, password, name, birth, address, role} = this.props.item;
            this.setState({id, username, password, name, birth, address, role});
        }
    }

    render() {
        return (
            <Form>

                <FormGroup>
                    <Label for="username">Username</Label>
                    <Input type="text" name="username" id="username" onChange={this.onChange}
                           value={this.state.username === null ? '' : this.state.username}/>
                </FormGroup>
                <FormGroup>
                    <Label for="password">Password</Label>
                    <Input type="text" name="password" id="password" onChange={this.onChange}
                           value={this.state.password === null ? '' : this.state.password}/>
                </FormGroup>
                <FormGroup>
                    <Label for="name">Name</Label>
                    <Input type="text" name="name" id="name" onChange={this.onChange}
                           value={this.state.name === null ? '' : this.state.name}/>
                </FormGroup>
                <FormGroup>
                    <Label for="birth">Birth</Label>
                    <Input type="date" name="birth" id="birth"
                           onChange={this.onChange}
                           value={this.state.birth === null ? '' : this.state.birth}
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="address">Address</Label>
                    <Input type="text" name="address" id="address" onChange={this.onChange}
                           value={this.state.address === null ? '' : this.state.address}/>
                </FormGroup>
                <FormGroup>
                    <Label for='role'>Role</Label>
                    <Select
                        options={this.state.roleData.map(item => ({value: item, label: item}))}
                        onChange={this.onChangeRole}
                        placeholder={this.state.role}
                    />
                </FormGroup>
                <Button onClick={this.props.item ? this.submitEditForm : this.submitAddForm}>Submit</Button>
            </Form>
        );
    }
}

export default PersonAddUpdateForm;