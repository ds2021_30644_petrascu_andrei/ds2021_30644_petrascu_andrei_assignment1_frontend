import React, {Component} from 'react'
import {Table, Button} from 'reactstrap';
import * as API_USERS from "../api/person-api";
import PersonModalForm from "./person-modal-form";


class PersonTable extends Component {

    constructor(props) {
        super(props);
        this.state = {}
    }

    deleteItem = id => {
        let confirmDelete = window.confirm('Delete item forever?');
        if (confirmDelete) {
            return API_USERS.remove(id, (result, status, error) => {
                if (result !== null && (status === 200 || status === 201)) {
                    console.log("Successfully deleted person with id: " + result);
                    this.props.reload();
                } else {
                    this.setState(({
                        errorStatus: status,
                        error: error
                    }));
                }
            });
        }
    }

    render() {

        const items = this.props.items.map(item => {
            return (
                <tr key={item.id}>
                    {/*<th scope="row">{item.id}</th>*/}
                    <td>{item.username}</td>
                    <td>{item.password}</td>
                    <td>{item.name}</td>
                    <td>{item.birth}</td>
                    <td>{item.address}</td>
                    <td>{item.role}</td>
                    <td>
                        <div style={{alignItems: "center", display: "flex"}}>
                            <PersonModalForm reload={this.props.reload} buttonLabel="Edit" item={item}/>
                            <Button color="danger" onClick={() => this.deleteItem(item.id)}>Delete</Button>
                        </div>
                    </td>
                </tr>
            )
        })

        return (
            <div>
                <Table hover bordered dark>
                    <thead>
                    <tr>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Name</th>
                        <th>Birth</th>
                        <th>Address</th>
                        <th>Role</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    {items}
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default PersonTable;