import React, {Component} from 'react'
import {Table, Button} from 'reactstrap';
import * as API_DEVICES from "./device-api";
import DeviceModalForm from "./device-modal-form";
import * as API_USERS from "../person/api/person-api";


class DeviceTable extends Component {

    constructor(props) {
        super(props);
        this.state = {}
    }

    deleteItem = id => {
        let confirmDelete = window.confirm('Delete item forever?');

        if (confirmDelete) {
            return API_DEVICES.remove(id, (result, status, error) => {
                if (result !== null && (status === 200 || status === 201)) {
                    console.log("Successfully deleted device with id: " + result);
                    this.props.reload();
                } else {
                    this.setState(({
                        errorStatus: status,
                        error: error
                    }));
                }
            });
        }
    }

    render() {

        const items = this.props.items.map(item => {
            return (
                <tr key={item.id}>
                    {/*<th scope="row">{item.id}</th>*/}
                    <td>{item.description}</td>
                    <td>{item.location}</td>
                    <td>{item.maxEnergyConsumption}</td>
                    <td>{item.mediumEnergyConsumption}</td>
                    <td>{item.person.name}</td>
                    <td>
                        <div style={{alignItems: "center", display: "flex"}}>
                            <DeviceModalForm reload={this.props.reload} buttonLabel="Edit" item={item}/>
                            <Button color="danger" onClick={() => this.deleteItem(item.id)}>Delete</Button>
                        </div>
                    </td>
                </tr>

            )
        })

        return (
            <div>
                <Table hover bordered dark>
                    <thead>
                    <tr>
                        <th>Description</th>
                        <th>Location</th>
                        <th>MaxEC</th>
                        <th>MediumEC</th>
                        <th>Person</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    {items}
                    </tbody>
                </Table>

            </div>
        )
    }
}

export default DeviceTable;