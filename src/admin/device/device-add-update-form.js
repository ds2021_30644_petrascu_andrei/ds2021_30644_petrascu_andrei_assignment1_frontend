import React from 'react';
import {Button, Form, FormGroup, Label, Input, Col} from 'reactstrap';
import Select from "react-select";
import * as API_USERS from "../person/api/person-api";
import * as API_DEVICES from "./device-api";

class DeviceAddUpdateForm extends React.Component {
    state = {
        id: 0,
        description: '',
        location: '',
        maxEnergyConsumption: '',
        mediumEnergyConsumption: '',
        person: {
            id: '',
            username: '',
            password: '',
            name: '',
            birth: '',
            address: '',
            role: ''
        },
        personData: []
    }

    onChange = item => {
        this.setState({[item.target.name]: item.target.value})
    }

    onChangePerson = item => {
        this.setState({person: item.value})
    }

    addDevice() {
        let item = {
            description: this.state.description,
            location: this.state.location,
            maxEnergyConsumption: this.state.maxEnergyConsumption,
            mediumEnergyConsumption: this.state.mediumEnergyConsumption,
            person: this.state.person
        };

        return API_DEVICES.post(item, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted device with id: " + result);
                //this.props.reload();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    editDevice() {
        let item = {
            id: this.state.id,
            description: this.state.description,
            location: this.state.location,
            maxEnergyConsumption: this.state.maxEnergyConsumption,
            mediumEnergyConsumption: this.state.mediumEnergyConsumption,
            person: this.state.person
        };

        return API_DEVICES.post(item, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    submitAddForm = e => {
        this.addDevice();
        this.props.toggle();
        this.props.reload();
    }

    submitEditForm = e => {
        this.editDevice();
        this.props.toggle();
        this.props.reload();
    }

    componentDidMount() {
        this.fetchPersons();
        /*if item exists => 'Edit' operation*/
        if (this.props.item) {
            const {id, description, location, maxEnergyConsumption, mediumEnergyConsumption, person} = this.props.item;
            this.setState({id, description, location, maxEnergyConsumption, mediumEnergyConsumption, person});
        }
    }

    fetchPersons() {
        return API_USERS.getPersons((result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    personData: result
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    render() {
        return (
            <Form>
                <FormGroup>
                    <Label for="description">Description</Label>
                    <Input type="text" name="description" id="description" onChange={this.onChange}
                           value={this.state.description === null ? '' : this.state.description}/>
                </FormGroup>
                <FormGroup>
                    <Label for="location">Location</Label>
                    <Input type="text" name="location" id="location" onChange={this.onChange}
                           value={this.state.location === null ? '' : this.state.location}/>
                </FormGroup>
                <FormGroup>
                    <Label for="maxEnergyConsumption">Max Energy Consumption</Label>
                    <Input type="number" name="maxEnergyConsumption" id="maxEnergyConsumption" onChange={this.onChange}
                           value={this.state.maxEnergyConsumption === null ? '' : this.state.maxEnergyConsumption}/>
                </FormGroup>
                <FormGroup>
                    <Label for="mediumEnergyConsumption">Medium Energy Consumption</Label>
                    <Input type="number" name="mediumEnergyConsumption" id="mediumEnergyConsumption"
                           onChange={this.onChange}
                           value={this.state.mediumEnergyConsumption === null ? '' : this.state.mediumEnergyConsumption}/>
                </FormGroup>

                <FormGroup id='person'>
                    <Label for='personField'>Person</Label>
                    <Select
                        options={this.state.personData.map(item => ({value: item, label: item.name}))}
                        onChange={this.onChangePerson}
                        placeholder={this.state.person.name}
                    />
                </FormGroup>
                <Button onClick={this.props.item ? this.submitEditForm : this.submitAddForm}>Submit</Button>
            </Form>
        );
    }
}

export default DeviceAddUpdateForm;