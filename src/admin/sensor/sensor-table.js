import React, {Component} from 'react'
import {Table, Button} from 'reactstrap';
import * as API_SENSORS from "./sensor-api";
import SensorModalForm from "./sensor-modal-form";

class SensorTable extends Component {

    constructor(props) {
        super(props);
        this.state = {}
    }

    deleteItem = id => {
        let confirmDelete = window.confirm('Delete item forever?');
        if (confirmDelete) {
            return API_SENSORS.remove(id, (result, status, error) => {
                if (result !== null && (status === 200 || status === 201)) {
                    console.log("Successfully deleted sensor with id: " + result);
                    this.props.reload();
                } else {
                    this.setState(({
                        errorStatus: status,
                        error: error
                    }));
                }
            });
        }
    }

    render() {

        const items = this.props.items.map(item => {
            return (
                <tr key={item.id}>
                    <td>{item.description}</td>
                    <td>{item.maxValue}</td>
                    <td>{item.currentValue}</td>
                    <td>{item.device.description}</td>
                    <td>
                        <div style={{alignItems: "center", display: "flex"}}>
                            <SensorModalForm reload={this.props.reload} buttonLabel="Edit" item={item}/>
                            <Button color="danger" onClick={() => this.deleteItem(item.id)}>Delete</Button>
                        </div>
                    </td>
                </tr>

            )
        })

        return (
            <div>
                <Table hover bordered dark>
                    <thead>
                    <tr>
                        <th>Description</th>
                        <th>MaxValue</th>
                        <th>CurrentValue</th>
                        <th>Device</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    {items}
                    </tbody>
                </Table>

            </div>
        )
    }
}

export default SensorTable;