import React from 'react';
import {Button, Form, FormGroup, Label, Input, Col} from 'reactstrap';
import Select from "react-select";
import * as API_SENSORS from "./sensor-api";
import * as API_DEVICES from "../device/device-api";

class SensorAddUpdateForm extends React.Component {
    state = {
        id: 0,
        description: '',
        maxValue: '',
        currentValue: '',
        device: '',
        deviceData: []
    }

    onChange = item => {
        this.setState({[item.target.name]: item.target.value})
    }

    onChangeDevice = item => {
        console.log(' on change device ' + item.description);
        this.setState({device: item.value}, () => {
            console.log(' on change device ' + this.state.device.id + ' ' + this.state.device.description)
        });
    }

    addSensor() {
        let item = {
            description: this.state.description,
            maxValue: this.state.maxValue,
            currentValue: this.state.currentValue,
            device: this.state.device
        };

        return API_SENSORS.post(item, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted SENSOR with id: " + result);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    editSensor() {
        let item = {
            id: this.state.id,
            description: this.state.description,
            maxValue: this.state.maxValue,
            currentValue: this.state.currentValue,
            device: this.state.device
        };

        return API_SENSORS.post(item, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    submitAddForm = e => {
        this.addSensor();
        this.props.toggle();
        this.props.reload();
    }

    submitEditForm = e => {
        this.editSensor();
        this.props.toggle();
        this.props.reload();
    }

    componentDidMount() {
        this.fetchDevices();
        /*if item exists => 'Edit' operation*/
        if (this.props.item) {
            const {id, description, maxValue, currentValue, device} = this.props.item;
            this.setState({id, description, maxValue, currentValue, device});
        }
    }

    fetchDevices() {
        return API_DEVICES.getAll((result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    deviceData: result
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    render() {
        return (
            <Form>
                <FormGroup>
                    <Label for="description">Description</Label>
                    <Input type="text" name="description" id="description" onChange={this.onChange}
                           value={this.state.description === null ? '' : this.state.description}/>
                </FormGroup>
                <FormGroup>
                    <Label for="maxValue">Max Value</Label>
                    <Input type="number" name="maxValue" id="maxValue" onChange={this.onChange}
                           value={this.state.maxValue === null ? '' : this.state.maxValue}/>
                </FormGroup>
                <FormGroup>
                    <Label for="currentValue">Current Value</Label>
                    <Input type="number" name="currentValue" id="currentValue" onChange={this.onChange}
                           value={this.state.currentValue === null ? '' : this.state.currentValue}/>
                </FormGroup>

                <FormGroup id='device'>
                    <Label for='deviceField'>Device</Label>
                    <Select
                        options={this.state.deviceData.map(item => ({value: item, label: item.description}))}
                        onChange={this.onChangeDevice}
                        placeholder={this.state.device.description}
                    />
                </FormGroup>
                <Button onClick={this.props.item ? this.submitEditForm : this.submitAddForm}>Submit</Button>
            </Form>
        );
    }
}

export default SensorAddUpdateForm;