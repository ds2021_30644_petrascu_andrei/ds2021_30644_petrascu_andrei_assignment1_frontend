import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = '/sensors';

function getAll(callback) {
    let request = new Request(HOST.backend_api + endpoint, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getById(params, callback) {
    let request = new Request(HOST.backend_api + endpoint + params.id, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function post(item, callback) {
    let request = new Request(HOST.backend_api + endpoint, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(item)
    });

    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function remove(id, callback) {
    let request = new Request(HOST.backend_api + endpoint + '/' + id, {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(id)
    });

    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getAll,
    getById,
    post,
    remove
};
