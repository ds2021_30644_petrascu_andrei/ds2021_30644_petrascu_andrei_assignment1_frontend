import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';

import * as API_SENSORS from "./sensor-api"
import SensorModalForm from "./sensor-modal-form";
import SensorTable from "./sensor-table";

class SensorContainer extends React.Component {

    constructor(props) {
        super(props);
        this.reload = this.reload.bind(this);
        this.state = {
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchSensors();
    }

    fetchSensors() {
        return API_SENSORS.getAll((result, status, err) => {
            if (result !== null && status === 200) {
                console.log(result);
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    reload() {
        this.setState({
            isLoaded: false
        });
        this.fetchSensors();
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Sensors Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '10', offset: 1}}>
                            <SensorModalForm reload={this.reload} buttonLabel="Add Sensor"/>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '10', offset: 1}}>
                            {this.state.isLoaded &&
                            <SensorTable reload={this.reload}
                                         items={this.state.tableData}
                            />}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Col>
                    </Row>
                </Card>
            </div>
        )

    }
}


export default SensorContainer;
