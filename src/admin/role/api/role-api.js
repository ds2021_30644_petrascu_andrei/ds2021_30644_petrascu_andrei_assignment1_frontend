import {HOST} from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";


const endpoint = {
    role: '/roles'
};

function getRoles(callback) {
    let request = new Request(HOST.backend_api + endpoint.role, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getRoles
};
