import React from 'react'
import logo from '../../commons/images/logo.png';
import {NavLink, useHistory} from 'react-router-dom';

import {
    Nav,
    Navbar,
    NavItem,
    NavbarBrand,
} from 'reactstrap';


const NavigationBar = ({handleRole}) => {

    let history = useHistory();

    const handleClick = () => {
        handleRole('');
        history.push('/');
    }

    return (
        <div>
            <Navbar color="light" light expand="md">
                <NavbarBrand href="/">
                    <img src={logo} width={"40"} height={"40"}/>
                </NavbarBrand>

                <Nav className="mr-auto" navbar>

                    <NavItem>
                        <NavLink to='/persons'>Persons</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink exact to='/devices'>__Devices</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink exact to='/sensors'>__Sensors</NavLink>
                    </NavItem>
                    <a onClick={handleClick}>__Logout</a>

                </Nav>
            </Navbar>
        </div>
    )
}

export default NavigationBar;
