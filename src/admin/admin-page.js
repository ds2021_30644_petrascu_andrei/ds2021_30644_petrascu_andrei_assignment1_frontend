import React from "react";
import NavigationBar from "./navbar/navigation-bar";
import {Switch, Route} from 'react-router-dom';
import PersonContainer from './person/person-container';
import DeviceContainer from "./device/device-container";
import SensorContainer from "./sensor/sensor-container";


class AdminPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loggedUser: ''
        }
    }

    componentDidMount() {
        this.setState({loggedUser: JSON.parse(localStorage.getItem('user'))});
    }

    render() {
        return (
            <div>
                <NavigationBar handleRole={this.props.handleRole}/>
                <p>Hello {this.state.loggedUser.name}</p>
                <Switch>
                    <Route path='/persons' component={PersonContainer}/>
                    <Route exact path='/devices' component={DeviceContainer}/>
                    <Route exact path='/sensors' component={SensorContainer}/>
                </Switch>
            </div>
        )
    }
}

export default AdminPage;