import React, {Component} from 'react';
import {
    Button,
    Form,
    FormGroup,
    Input,
    Label
} from 'reactstrap';
import './login-style.css';
import * as API_LOGIN from './login-api';
import * as API_USERS from "../admin/person/api/person-api";

class LoginContainer extends Component {

    state = {
        user: {
            username: 'johndoe',
            password: '*******'
        },
        loggedUser: ''
    }

    handleSubmit() {

    }

    onChange = e => {
        const name = e.target.name;
        const value = e.target.value;

        const updatedUser = this.state.user;
        updatedUser[name] = value;

        this.setState({user: updatedUser});
    }

    onSubmit = e => {
        API_LOGIN.post(this.state.user, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {

                const loggedUser = JSON.stringify(result);
                localStorage.setItem('user', loggedUser);

                if (result.role === 'ADMIN') {
                    this.props.role(result.role);
                } else if (result.role === 'CLIENT') {
                    this.props.role(result.role);
                }
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });

    }

    render() {
        return (
            <div className="App">

                <div className="alert alert-info">
                    <strong>Normal User</strong> - U: user P: user<br/>
                    <strong>Administrator</strong> - U: admin P: admin
                </div>

                <h2>Sign In</h2>
                <Form className="form">
                    <FormGroup>
                        <Label for="exampleUsername">Username</Label>
                        <Input
                            type="text"
                            name="username"
                            id="exampleUsername"
                            placeholder={this.state.user.username}
                            onChange={this.onChange}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="examplePassword">Password</Label>
                        <Input
                            type="password"
                            name="password"
                            id="examplePassword"
                            placeholder={this.state.user.password}
                            onChange={this.onChange}
                        />
                    </FormGroup>
                    <Button onClick={this.onSubmit}>Submit</Button>
                </Form>
            </div>
        );
    }
}

export default LoginContainer;