import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {
    Card,
    CardHeader,
    Col,
    Row
} from 'reactstrap';
import * as API_MYDEVICES from "./mydevices-api";
import MyDevicesTable from "./mydevices-table";


class MyDevicesContainer extends React.Component {

    constructor(props) {
        super(props);
        this.reload = this.reload.bind(this);
        this.loggedUser = JSON.parse(localStorage.getItem('user'));
        this.state = {
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
            selectedDevice: '',
            energies: []
        };
    }

    componentDidMount() {
        this.fetchDevices();
    }

    fetchDevices() {
        return API_MYDEVICES.getClientDevices(this.loggedUser.id, (result, status, err) => {
            if (result !== null && status === 200) {
                console.log(result);
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    reload() {
        this.setState({
            isLoaded: false
        });
        this.fetchDevices();
    }

    updateSelectedDevice = (selectedDevice) => {
        this.setState({selectedDevice})
    }

    updateEnergies = (e) => {
        this.setState({energies: e})
    }

    render() {

        const energyData = this.state.energies.map(item => {
            return (<p>Timestamp: {item.timestamp}    Consumption: {item.consumption} </p>)
        })

        return (
            <div>
                <CardHeader>
                    <strong>My Devices [{this.loggedUser.name}]</strong>
                </CardHeader>
                <Card>
                    <Row>
                        <Col sm={{size: '10', offset: 1}}>
                            {this.state.isLoaded &&
                            <MyDevicesTable energies={this.updateEnergies}
                                            selectedDevice={this.updateSelectedDevice}
                                            items={this.state.tableData}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                            <p>The current energy for selected device [{this.state.selectedDevice.description}]
                                is : {this.state.selectedDevice.currentValue} Kwh</p>
                            <p>Historical Energy for selected device : </p>
                            {energyData}

                        </Col>
                    </Row>
                </Card>
            </div>
        )

    }
}


export default MyDevicesContainer;
