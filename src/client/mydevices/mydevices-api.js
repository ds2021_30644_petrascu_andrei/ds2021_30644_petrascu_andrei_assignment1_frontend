import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    persons: '/persons',
    clientDevices: '/getPersonDevices',
    devices: '/devices',
    deviceSensor: '/getDeviceSensor',
    deviceEnergies: '/getDeviceEnergies',
    energiesAsMap: '/getEnergiesAsMap'
}

function getClientDevices(id, callback) {
    let request = new Request(HOST.backend_api + endpoint.persons + endpoint.clientDevices + '/' + id, {
        method: 'GET',
    });
    RestApiClient.performRequest(request, callback);
}

function getDeviceSensor(id, callback) {
    let request = new Request(HOST.backend_api + endpoint.devices + endpoint.deviceSensor + '/' + id, {
        method: 'GET',
    });
    RestApiClient.performRequest(request, callback);
}

function getDeviceEnergies(id, callback) {
    let request = new Request(HOST.backend_api + endpoint.devices + endpoint.deviceEnergies + '/' + id, {
        method: 'GET',
    });
    RestApiClient.performRequest(request, callback);
}

function getEnergiesAsMap(id, callback) {
    let request = new Request(HOST.backend_api + endpoint.persons + endpoint.energiesAsMap + '/' + id, {
        method: 'GET',
    });
    RestApiClient.performRequest(request, callback);
}

export {
    getClientDevices,
    getDeviceSensor,
    getDeviceEnergies,
    getEnergiesAsMap
};
