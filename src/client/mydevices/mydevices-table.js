import React, {Component} from 'react'
import {Table, Button} from 'reactstrap';
import * as API_MYDEVICES from "./mydevices-api";

class MyDevicesTable extends Component {

    constructor(props) {
        super(props);
    }

    showCurrentEnergy = (id) => {
        return API_MYDEVICES.getDeviceSensor(id, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                this.props.selectedDevice(result);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    showHistoricalEnergy = (id) => {
        return API_MYDEVICES.getDeviceEnergies(id, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                this.props.energies(result);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    render() {
        const items = this.props.items.map(item => {
            return (
                <tr key={item.id}>
                    <td>{item.description}</td>
                    <td>{item.location}</td>
                    <td>{item.maxEnergyConsumption}</td>
                    <td>{item.mediumEnergyConsumption}</td>
                    <td>{item.person.name}</td>
                    <td>
                        <div style={{justifyContent: "space-around", alignItems: "center", display: "flex"}}>
                            <Button color="secondary" onClick={() => this.showCurrentEnergy(item.id)}>CurrentEnergy</Button>
                            <Button color="info" onClick={() => this.showHistoricalEnergy(item.id)}>HistoricalEnergy</Button>
                        </div>
                    </td>
                </tr>

            )
        })

        return (
            <div>
                <Table hover bordered light="true">
                    <thead>
                    <tr>
                        <th>Description</th>
                        <th>Location</th>
                        <th>MaxEC</th>
                        <th>MediumEC</th>
                        <th>Person</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    {items}
                    </tbody>
                </Table>

            </div>
        )
    }
}

export default MyDevicesTable;