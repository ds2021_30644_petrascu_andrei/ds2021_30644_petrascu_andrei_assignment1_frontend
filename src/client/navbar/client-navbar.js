import React from 'react'
import logo from '../../commons/images/logo.png';
import {NavLink, useHistory} from 'react-router-dom';

import {
    Nav,
    Navbar,
    NavItem,
    NavbarBrand,
} from 'reactstrap';


const ClientNavbar = ({handleRole}) => {

    let history = useHistory();

    const handleClick = () => {
        handleRole('');
        history.push('/');
    }

    return (
        <div>
            <Navbar color="light" light expand="md">
                <NavbarBrand href="/">
                    <img src={logo} width={"40"} height={"40"}/>
                </NavbarBrand>
                <Nav className="mr-auto" navbar>
                    <NavLink exact to='/myDevices'>MyDevices </NavLink>
                    <NavLink exact to='/mySensors'>__MySensors</NavLink>
                    <NavLink exact to='/myCharts'>__MyCharts</NavLink>
                    <a onClick={handleClick}>__Logout</a>
                </Nav>
            </Navbar>
        </div>
    )
}

export default ClientNavbar;
