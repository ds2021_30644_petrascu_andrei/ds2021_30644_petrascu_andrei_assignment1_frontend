import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    sensors: '/persons',
    clientSensors: '/getPersonSensors'
}

function getClientSensors(id, callback) {
    let request = new Request(HOST.backend_api + endpoint.sensors + endpoint.clientSensors + '/' + id, {
        method: 'GET',
    });
    RestApiClient.performRequest(request, callback);
}

export {
    getClientSensors
};
