import React, {Component} from 'react'
import {Table} from 'reactstrap';

class MySensorsTable extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const items = this.props.items.map(item => {
            return (
                <tr key={item.id}>
                    <td>{item.description}</td>
                    <td>{item.maxValue}</td>
                    <td>{item.currentValue}</td>
                    <td>{item.device.description}</td>
                </tr>

            )
        })

        return (
            <div>
                <Table hover bordered light="true">
                    <thead>
                    <tr>
                        <th>Description</th>
                        <th>MaxValue</th>
                        <th>CurrentValue</th>
                        <th>Device</th>
                    </tr>
                    </thead>
                    <tbody>
                    {items}
                    </tbody>
                </Table>

            </div>
        )
    }
}

export default MySensorsTable;