import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {
    Card,
    CardHeader,
    Col,
    Row
} from 'reactstrap';
import * as API_SENSORS from "./mysensors-api";
import MySensorsTable from "./mysensors-table";

class MySensorsContainer extends React.Component {

    constructor(props) {
        super(props);
        this.reload = this.reload.bind(this);
        this.loggedUser = JSON.parse(localStorage.getItem('user'));
        this.state = {
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchSensors();
    }

    fetchSensors() {
        return API_SENSORS.getClientSensors(this.loggedUser.id, (result, status, err) => {
            if (result !== null && status === 200) {
                console.log(result);
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    reload() {
        this.setState({
            isLoaded: false
        });
        this.fetchSensors();
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong>My Sensors [{this.loggedUser.name}]</strong>
                </CardHeader>
                <Card>
                    <Row>
                        <Col sm={{size: '10', offset: 1}}>
                            {this.state.isLoaded &&
                            <MySensorsTable items={this.state.tableData}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Col>
                    </Row>
                </Card>
            </div>
        )
    }
}


export default MySensorsContainer;
