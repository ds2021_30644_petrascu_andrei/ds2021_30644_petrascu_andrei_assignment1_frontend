import React from "react";
import ClientNavbar from "./navbar/client-navbar";
import {Switch, Route} from 'react-router-dom';
import MyDevicesContainer from "./mydevices/mydevices-container";
import MySensorsContainer from "./mysensors/mysensors-container";
import MyChartsContainer from "./mychart/mycharts-container";

class ClientPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loggedUser: ''
        }
    }

    componentDidMount() {
        this.setState({loggedUser: JSON.parse(localStorage.getItem('user'))});
    }

    render() {
        return (
            <div>
                <ClientNavbar handleRole={this.props.handleRole}/>
                <Switch>
                    <Route path='/myDevices' component={MyDevicesContainer}/>
                    <Route path='/mySensors' component={MySensorsContainer}/>
                    <Route path='/myCharts' component={MyChartsContainer}/>
                </Switch>
            </div>
        )
    }
}

export default ClientPage;