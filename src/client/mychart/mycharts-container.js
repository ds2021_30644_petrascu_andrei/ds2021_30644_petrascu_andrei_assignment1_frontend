import React from 'react';
import {Line} from "react-chartjs-2";
import {MDBContainer} from "mdbreact";
import {
    Card,
    CardHeader,
    Col, FormGroup, Input, Label,
    Row
} from 'reactstrap';
import * as API_MYDEVICES from "../mydevices/mydevices-api";

class MyChartsContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            dataLine: {
                labels: ["00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"],
                datasets: [
                    {
                        label: "My First dataset",
                        fill: true,
                        lineTension: 0.3,
                        backgroundColor: "rgba(225, 204,230, .3)",
                        borderColor: "rgb(205, 130, 158)",
                        borderCapStyle: "butt",
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: "miter",
                        pointBorderColor: "rgb(205, 130,1 58)",
                        pointBackgroundColor: "rgb(255, 255, 255)",
                        pointBorderWidth: 10,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgb(0, 0, 0)",
                        pointHoverBorderColor: "rgba(220, 220, 220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: [10, 20, 12, 45, 6, 44, 23, 8, 33, 14, 5, 29, 30, 44, 32, 11, 45, 9, 23, 21, 32, 13, 21, 22]
                    }
                ]
            }
        };
    }

    onChange = item => {
        return API_MYDEVICES.getEnergiesAsMap((JSON.parse(localStorage.getItem('user'))).id, (result, status, err) => {
            if (result !== null && status === 200) {

                const dataline = this.state.dataLine;

                for (let i = 0; i <= 23; i++) {
                    dataline.labels[i] = result.labels[i];
                    dataline.datasets[0].data[i] = result.data[i];
                }

                this.setState({dataLine: dataline});

            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong>My Charts</strong>
                </CardHeader>
                <Card>
                    <Row>
                        <Col sm={{size: '2', offset: 1}}>
                            <FormGroup>
                                <Label for="day">Day</Label>
                                <Input type="date" name="day" id="day"
                                       onChange={this.onChange}
                                />
                            </FormGroup>
                        </Col>
                        <MDBContainer>
                            <h3 className="mt-5">Line chart</h3>
                            <Line data={this.state.dataLine} options={{responsive: true}}/>
                        </MDBContainer>
                    </Row>
                </Card>
            </div>
        )
    }
}

export default MyChartsContainer;
