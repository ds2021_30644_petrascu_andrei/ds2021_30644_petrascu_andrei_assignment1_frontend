import React from 'react'
import {BrowserRouter as Router, Route, Switch,} from 'react-router-dom'
import NavigationBar from './admin/navbar/navigation-bar'
import PersonContainer from './admin/person/person-container'
import SensorContainer from './admin/sensor/sensor-container'
import DeviceContainer from './admin/device/device-container'

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';
import LoginContainer from "./login/login-container";
import AdminPage from "./admin/admin-page";
import ClientPage from "./client/client-page";

class App extends React.Component {

    state = {
        role: ''
    }

    handleRole = (role) => {
        this.setState({role})
    }

    render() {
        return (
            <div className={styles.back}>
                <Router>
                    <div>
                        <Switch>
                             {
                                this.state.role == 'ADMIN' ?
                                    <Route path='/' render={() => <AdminPage handleRole={this.handleRole}/>}/>
                                    :
                                    (this.state.role == 'CLIENT' ?
                                        <Route path='/' render={() => <ClientPage handleRole={this.handleRole}/>}/> :
                                        <Route exact path='/' render={() => <LoginContainer role={this.handleRole}/>}/>)
                            }

                           {/* {
                                (JSON.parse(localStorage.getItem('user'))).role == 'ADMIN' ?
                                    <Route path='/' render={() => <AdminPage handleRole={this.handleRole}/>}/>
                                    :
                                    ((JSON.parse(localStorage.getItem('user'))).role == 'CLIENT' ?
                                        <Route path='/' render={() => <ClientPage handleRole={this.handleRole}/>}/> :
                                        <Route exact path='/' render={() => <LoginContainer role={this.handleRole}/>}/>)
                            }*/}


                            {/*Error*/}
                            <Route
                                exact
                                path='/error'
                                render={() => <ErrorPage/>}
                            />

                            <Route render={() => <ErrorPage/>}/>
                        </Switch>
                    </div>
                </Router>
            </div>
        )
    };
}

export default App
